var x = document.getElementById("div1");

var emaillabel = document.createElement('label'); // Create Label for E-mail Field
emaillabel.innerHTML = "Your Email : ";
x.appendChild(emaillabel);

var emailelement = document.createElement('input'); // Create Input Field for E-mail
emailelement.setAttribute("type", "text");
emailelement.setAttribute("name", "demail");
x.appendChild(emailelement);

var submitelement = document.createElement('input'); // Append Submit Button
submitelement.setAttribute("type", "submit");
submitelement.setAttribute("name", "dsubmit");

x.appendChild(submitelement);


submitelement.onclick = function() { ValidateEmail(emailelement) };

emailelement.onfocus = function() { handleonfocus(emailelement)};
emailelement.onhover = function() { handleonhover(emailelement)};

function handleonfocus(x){
    x.style.color = "red";
    x.style.backgroundcolor = "green";
}

function handleonhover(x ){
    x.style.color = "green";
    x.style.backgroundcolor = "red";
}   
function ValidateEmail(inputText) {

    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.value.match(mailformat)) {
        alert("you have entered an valid email address");
        emailcheck(emailelement);
    }
    else {
        alert("You have entered an invalid email address!");
    }
}

function emailcheck(emailelement) {
    var res = emailelement.value;
    var str = res.replace(/.*@/, "");
    if (str.toLowerCase() === "consultadd.com") {
        alert("valid email");
        console.log(true);
    }
    else {
        alert("invalid email");
        console.log(false);
    }
}

submitelement.onclick = async () => {
    console.log('Button was clicked')
    try {
        const response = await fetch('https://demo7857661.mockable.io/testdata')
        const result = await response.json()
        localStorage.setItem('name', result.firstName)
        //localStorage.setItem('age', result.age)
        sessionStorage.setItem('age', result.age);
        window.location = 'home.html' 
    }catch(err) {
        console.log(err.message)
    }
}